#include "Log.h"
#include <cstdarg>
#include <ctime>

#define LOG_BUFFER_SIZE     1000
#define ARGS_TO_CHAR_ARRAY(X,Y,W) va_list Z; va_start ((Z), X); vsnprintf ((Y), (W), (X), (Z)); va_end ((Z))

std::string Log::getCurrentDateTime() {
    time_t     now = time(nullptr);
    struct tm  tstruct = {};
    char       buf[80];
    tstruct = *localtime( &now);
    strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);
    return buf;
}

void Log::info(const std::string& msg) {
    printf("[%s][INFO]: %s\n", getCurrentDateTime().c_str(), msg.c_str());
}

void Log::info(const char *format, ...) {
    char buf[LOG_BUFFER_SIZE];
    ARGS_TO_CHAR_ARRAY(format, buf, LOG_BUFFER_SIZE);
    info(std::string(buf));
}

void Log::warning(const std::string& msg) {
    printf("[%s][WARNING]: %s\n", getCurrentDateTime().c_str(), msg.c_str());
}

void Log::warning(const char *format, ...) {
    char buf[LOG_BUFFER_SIZE];
    ARGS_TO_CHAR_ARRAY(format, buf, LOG_BUFFER_SIZE);
    warning(std::string(buf));
}

void Log::error(const std::string& msg) {
    printf("[%s][ERROR]: %s\n", getCurrentDateTime().c_str(), msg.c_str());
}

void Log::error(const char *format, ...) {
    char buf[LOG_BUFFER_SIZE];
    ARGS_TO_CHAR_ARRAY(format, buf, LOG_BUFFER_SIZE);
    error(std::string(buf));
}

void Log::debug(const std::string& msg) {
    printf("[%s][DEBUG]: %s\n", getCurrentDateTime().c_str(), msg.c_str());
}

void Log::debug(const char *format, ...) {
    char buf[LOG_BUFFER_SIZE];
    ARGS_TO_CHAR_ARRAY(format, buf, LOG_BUFFER_SIZE);
    debug(std::string(buf));
}

void Log::printStackTrace(const std::exception &e, int level) {
	if (level==0) {
		error("%s", e.what());
	}
	else {
		printf("		%s\n", e.what());
	}
	try {
		std::rethrow_if_nested(e);
	} catch(const std::exception& e) {
		printStackTrace(e, level+1);
	} catch(...) {}
}
